#include <SPI.h>
//#include <SD.h>

#include <UTFTGLUE.h>
#include "Inconsolata8pt7b.h"

/*

  Padre y Señor
  del Bosque,

  Abuelo
  de barbas
  vegetales,

  Yo quisiera
  mi canto como
  torre

  para poder
  alzarla en
  tu homenaje;

*/


UTFTGLUE myGLCD(0, A2, A1, A3, A4, A0);


//#define SD_CS     10


bool hayPoema = false;
//File poema;

const unsigned int N_PARRAFOS = 2;

const int w = 480;
const int h = 320;


char separadorLinea = '\n';

unsigned int paddingLeft = 35;
unsigned int paddingTop = 90;
unsigned int espacioLinea = 60;
float textSize = 4;

const int DELAY = 5000;

char* s = "Padre y Señor   del Bosque,";
void setup() {
  randomSeed(analogRead(0));
  pinMode(A5, INPUT_PULLUP);

  Serial.begin(9600);

  while (!Serial) {
  }


  // Setup the LCD
  myGLCD.InitLCD();
  //  myGLCD.setContrast(char (5));
  myGLCD.setFont(&Inconsolata_Regular8pt8b);
  myGLCD.clrScr();

  myGLCD.setTextSize(textSize);
  utf8ascii(s);
}

void leerPoema() {
  hayPoema = true;
  myGLCD.setColor(0, 0, 0);
  myGLCD.fillRect(0, 0, 480, 320);
  
  if (hayPoema) {

    //TEXTO
    myGLCD.setColor(250, 250, 250);
    myGLCD.setTextSize(textSize);

    myGLCD.print(s, paddingLeft, paddingTop);
    delay(DELAY);
    myGLCD.setColor(0, 0, 0);
    myGLCD.fillRect(0, 0, 480, 320);
    myGLCD.setColor(250, 250, 250);
    myGLCD.print("Abuelo           de barbas        vegetales,", paddingLeft, paddingTop);
    delay(DELAY);
    myGLCD.setColor(0, 0, 0);
    myGLCD.fillRect(0, 0, 480, 320);
    myGLCD.setColor(250, 250, 250);
    myGLCD.print("Yo quisiera      mi canto        como torre", paddingLeft, paddingTop);
    delay(DELAY);
    myGLCD.setColor(0, 0, 0);
    myGLCD.fillRect(0, 0, 480, 320);
    myGLCD.setColor(250, 250, 250);
    myGLCD.print("para poder      alzarla en      tu homenaje", paddingLeft, paddingTop);
    delay(DELAY);
    //disolve();
    myGLCD.setColor(0, 0, 0);
    myGLCD.fillRect(0, 0, 480, 320);
    hayPoema = false;
  }
}



void loop() {
  if (digitalRead(A5)) {
    
  } else {
    leerPoema();
  }

}


//TODO LO QUE SIGUE ES PARA QUE LA PANTALLA MUESTRE BIEN LAS Ñ

// ****** UTF8-Decoder: convert UTF8-string to extended ASCII *******
static byte c1;  // Last character buffer

// Convert a single Character from UTF8 to Extended ASCII
// Return "0" if a byte has to be ignored
byte utf8ascii(byte ascii) {
  if ( ascii < 128 ) // Standard ASCII-set 0..0x7F handling
  { c1 = 0;
    return ( ascii );
  }

  // get previous input
  byte last = c1;   // get last char
  c1 = ascii;       // remember actual character

  switch (last)     // conversion depending on first UTF8-character
  { case 0xC2: return  (ascii);  break;
    case 0xC3: return  (ascii | 0xC0);  break;
    case 0x82: if (ascii == 0xAC) return (0x80);   // special case Euro-symbol
  }

  return  (0);                                     // otherwise: return zero, if character has to be ignored
}

// convert String object from UTF8 String to Extended ASCII
String utf8ascii(String s)
{
  String r = "";
  char c;
  for (int i = 0; i < s.length(); i++)
  {
    c = utf8ascii(s.charAt(i));
    if (c != 0) r += c;
  }
  return r;
}

// In Place conversion UTF8-string to Extended ASCII (ASCII is shorter!)
void utf8ascii(char* s)
{
  int k = 0;
  char c;
  for (int i = 0; i < strlen(s); i++)
  {
    c = utf8ascii(s[i]);
    if (c != 0)
      s[k++] = c;
  }
  s[k] = 0;
}
