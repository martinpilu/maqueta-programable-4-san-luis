import processing.serial.*;

import controlP5.*;

ControlP5 cP5;
ListBox l;

Serial myPort;

PrintWriter output1;

int val;
boolean puertoAbierto;
int posicion[] = new int[5];
int CANTIDAD = 11;
String[] lines;
int index = 0;

String[][] valores = new String[12][6];
String path = null;

char brazoActivo;

void setup() {
  size(400, 800);
  cP5 = new ControlP5(this);
  selectInput("abrir", "fileSelected");
  //lines = loadStrings("cargaA.txt");
  while (path==null) {
    println("esperando carga de archivo");
  };
  lines = loadStrings(path);
  for (int i=0; i<lines.length; i++) {
    // println(lines.length);
    if (i>0 && i < lines.length) {
      String[] axis = (splitTokens(lines[i], ","));
      for (int j=0; j<axis.length; j++) {
        axis[j] = axis[j].replace("{", "");
        axis[j] = axis[j].replace("}", "");
        axis[j] = axis[j].replace(" ", "");
        println(axis[j]);
        valores[i-1][j] = axis[j];
      }
    }
  }


  for (int i=0; i<CANTIDAD; i++) {
    String str1 = "base_";
    str1 = str1 + i;
    cP5.addTextfield(str1)
      .setPosition(20, 30 + i*35)
      .setSize(50, 20)
      .setFocus(false)
      .setColor(color(255, 0, 0))
      .setText(valores[i][0])
      ;
    str1 = "hombro_";
    str1 = str1 + i;
    cP5.addTextfield(str1)
      .setPosition(75, 30 + i*35)
      .setSize(50, 20)
      .setFocus(false)
      .setColor(color(255, 0, 0))
      .setText(valores[i][1])
      ;
    str1 = "codo_";
    str1 = str1 + i;
    cP5.addTextfield(str1)
      .setPosition(130, 30 + i*35)
      .setSize(50, 20)
      .setFocus(false)
      .setColor(color(255, 0, 0))
      .setText(valores[i][2])
      ;
    str1 = "munieca_";
    str1 = str1 + i;
    cP5.addTextfield(str1)
      .setPosition(185, 30 + i*35)
      .setSize(50, 20)
      .setFocus(false)
      .setColor(color(255, 0, 0))
      .setText(valores[i][3])
      ;
    str1 = "garra_";
    str1 = str1 + i;
    cP5.addTextfield(str1)
      .setPosition(240, 30 + i *35)
      .setSize(50, 20)
      .setFocus(false)
      .setColor(color(255, 0, 0))
      .setText(valores[i][4])
      ;
    str1 = "enviar_";
    str1 = str1 + i;
    cP5.addBang(str(i))
      .setPosition(295, 30 + i*35)
      .setSize(50, 20)
      .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER)
      ;
  }

  cP5.addBang("guardar_1")
    .setPosition(20, 420)
    .setSize(50, 40)
    .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER)
    ;


  for (int i=0; i<5; i++) {
    posicion[i] = 0;
  }



  l = cP5.addListBox("puerto serie", 10, 10, 350, 150);
  l.setItemHeight(23);
  l.close();

  for (int i=0; i<Serial.list().length; i++) {
    l.addItem(Serial.list()[i], i);
  }
  puertoAbierto = false;
  brazoActivo = 'A';
}

void draw() {
  background(0);
  textSize(32);
  fill(255);
  text(brazoActivo, 80, 460); 
  if (puertoAbierto) {
    if ( myPort.available() > 0) {  // If data is available,
      val = myPort.read();         // read it and store it in val
      println(val);
    }
  }
}

void keyPressed() {
  if (key == 'a' || key == 'A') {
    //A;
    brazoActivo = 'A';
    
  }
  if (key == 'b' || key == 'B') {
    //B;
    brazoActivo = 'B';
  }
  if (key == 'c' || key == 'C') {
    //C;
    brazoActivo = 'C';
  }
}

//void guardar_1() {

//  println("holaqueta;");
//}

public void controlEvent(ControlEvent theEvent) {
  if (theEvent.isGroup()) {
    print("> "+theEvent.getGroup().getValue());
    int n = int(theEvent.getGroup().getValue());
    println("\t\t group:"+(n >> 8 & 0xff)+", item:"+(n >> 0 & 0xff));
  } else if (theEvent.isController()) {
    println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
    if (theEvent.getController() == l) {
      myPort = new Serial(this, Serial.list()[int(theEvent.getController().getValue())], 9600);
      puertoAbierto = true;
      l.close();
    } else if (theEvent.getLabel() == "guardar_1") {
      salvarTXT1();
    } else {
      poneLasCosas(theEvent.getController().getLabel());
    }
  }
}

void poneLasCosas (String dato) {
  String btnAddress = dato;
  println(btnAddress);
  String base = "base_";
  base = base + btnAddress;
  println(base);
  String hombro = "hombro_";
  hombro = hombro + btnAddress;
  String codo = "codo_";
  codo = codo + btnAddress;
  String munieca = "munieca_";
  munieca = munieca + btnAddress;
  String garra = "garra_";
  garra = garra + btnAddress;
  posicion[0] = int((cP5.get(Textfield.class, base).getText()));
  posicion[1] = int((cP5.get(Textfield.class, hombro).getText()));
  posicion[2] = int((cP5.get(Textfield.class, codo).getText()));
  posicion[3] = int((cP5.get(Textfield.class, munieca).getText()));
  posicion[4] = int((cP5.get(Textfield.class, garra).getText()));

  switch(brazoActivo) {
  case 'A': 
    myPort.write("~#A");
    break;
  case 'B':
    myPort.write("~#B");
    break;
  case 'C':
    myPort.write("~#C");
    break;
  }
  myPort.write(posicion[0]);
  myPort.write(posicion[1]);
  myPort.write(posicion[2]);
  myPort.write(posicion[3]);
  myPort.write(posicion[4]);
}

void salvarTXT1 () {
  output1 = createWriter(path);
  output1.println('{'); // Write the coordinate to the file
  for (int i=0; i<CANTIDAD; i++) {
    output1.print('{');
    String btnAddress = str(i);
    println(btnAddress);
    String base = "base_";
    base = base + btnAddress;
    println(base);
    String hombro = "hombro_";
    hombro = hombro + btnAddress;
    String codo = "codo_";
    codo = codo + btnAddress;
    String munieca = "munieca_";
    munieca = munieca + btnAddress;
    String garra = "garra_";
    garra = garra + btnAddress;
    posicion[0] = int((cP5.get(Textfield.class, base).getText()));
    posicion[1] = int((cP5.get(Textfield.class, hombro).getText()));
    posicion[2] = int((cP5.get(Textfield.class, codo).getText()));
    posicion[3] = int((cP5.get(Textfield.class, munieca).getText()));
    posicion[4] = int((cP5.get(Textfield.class, garra).getText()));
    output1.print(posicion[0]);
    output1.print(", ");
    output1.print(posicion[1]);
    output1.print(", ");
    output1.print(posicion[2]);
    output1.print(", ");
    output1.print(posicion[3]);
    output1.print(", ");
    output1.print(posicion[4]);
    output1.print("}");
    if (i<CANTIDAD-1) {
      output1.println(", ");
    } else {
      output1.println();
    }
  }
  output1.print('}');
  output1.flush(); // Writes the remaining data to the file
  output1.close();
}

void fileSelected(File archivo) {
  if (archivo==null) {
    //path="default.txt";
    println("no se cargo ningun archivo de datos");
  } else {
    path = archivo.getAbsolutePath();
    println(path);
  }
}
