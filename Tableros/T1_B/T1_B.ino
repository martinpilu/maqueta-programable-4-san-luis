

/*
   Tablero 1, placa B.
   ver: 1.0.0
   Acciones:
   -- Productividad
   38 Camion
   39 Balanza
   40 Tanquera (i2c 0)
   41 Regadores
   --- LABO
   55 Apertura de puerta corrediza (i2c 2)
   56 Encendido de Luz gral en laboratorio
   57 Encendido del reactor giratorio (planta baja)
   58 Encendido de máquinas en planta baja
   59 Encendido de máquinas en primer piso

   salidas exp labo
   0.NC
   1.LUZ PB
   2.LUZ PA
   3.MAQ PB A
   4.MAQ PB B
   5.MAQ PA A / BPP
   6.MOT REACTOR
   7 a 15 NC


*/
#include <Gallardcore_NanoExpander.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include "Adafruit_MCP23017.h"
#include <Adafruit_NeoPixel_NOT.h>
#include <avr/wdt.h>


#define PXLED_PIN 7 //Out 1

#define LED_COUNT 7 //6 reactor, 1 maq PA

Adafruit_MCP23017 Exp_brd;

Adafruit_PWMServoDriver Srv = Adafruit_PWMServoDriver();

NANO_EXPANDER Brd;

Adafruit_NeoPixel px1(LED_COUNT, PXLED_PIN, NEO_BRG + NEO_KHZ800);

//sistewma desplazamiento (pin fin, pin inicio, out puente H, out puente H, velocidad)
Desplazamiento camion(A1, A0, OUT4, OUT5, 255);

uint8_t actionState[9] = {0, 0, 0, 0, 3, 0, 0, 0, 0};
static uint8_t actionNumber[9] = {38, 39, 40, 41, 55, 56, 57, 58, 59};

#define REG_SPEED 50

unsigned long timer = 0;
const unsigned int frameTime = 100;
unsigned long frame = 0;
#define TRANQUERA_MAX 520
#define TRANQUERA_MIN 250

#define SRV_CONT_MAX 200
#define SRV_CONT_MIN 500
#define SRV_CONT_MID 350
#define T_SRV_CONT 900
//uint8_t prevStateSrvCont = actionState [4];
unsigned long timerSrvCont;

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif
  //exp
  Exp_brd.begin();      // use default address 0
  Brd.init();
  for (int i = 0; i < 16; i++) {
    Exp_brd.pinMode(i, OUTPUT);
  }

  // servo
  Srv.begin();
  Srv.setPWMFreq(60);

  //px out1
  px1.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px1.setBrightness(255); //
  px1.show();            // Turn OFF all pixels ASAP
  for (int i = 0; i < 7; i++) {
    px1.setPixelColor(i, px1.Color(255,   255,   255));
  }
  px1.show();
  delay(2000);
  timerSrvCont = millis();
  Srv.setPWM(2, 0, 350);
  wdt_enable(WDTO_8S);
  Serial.println("SETUP DONE");
}

void loop() {
  //Serial.println(millis());
  wdt_reset();
  Serial.println("start");
  frameCounter();
  // leer mensajes del server.
  Serial.println("parse");
  Brd.parseSerial(actionState, actionNumber, 9);
  Serial.println("camion");
  camion.tick();

  // camion actionState[0]
  if (actionState[0] > 0) {
    actionState[0] = 0;
    //si tranquera abierta.
    if (actionState[2]) camion.go();
  }
  Serial.println("balanza");
  //balanza
  Brd.setOut(6, actionState[1] * 255);
  Serial.println("tranquera");
  //tranquera actionState[2] y camion andando.
  if (actionState[2] || camion.state() > 0) {
    Srv.setPWM(0, 0, TRANQUERA_MAX);
  } else {
    Srv.setPWM(0, 0, TRANQUERA_MIN);
  }
  Serial.println("regadores");
  //regadores
  Brd.setOut(1, actionState[3] * REG_SPEED);
  Brd.setOut(2, actionState[3] * REG_SPEED);
  Brd.setOut(3, actionState[3] * REG_SPEED);

  Serial.println("labo");
  //LABO
  //puerta actionState[4]
  //s 0 = mover cerrar y cont
  //s1 mover abrir y cont
  //s2 esprar y parar
  //s3 idle
  Serial.println("puerta");
  switch (actionState[4]) {
    case 0:
      timerSrvCont = millis();
      Srv.setPWM(2, 0, SRV_CONT_MIN);
      actionState[4] = 2;
      break;
    case 1:
      timerSrvCont = millis();
      Srv.setPWM(2, 0, SRV_CONT_MAX);
      actionState[4] = 2;
      break;
    case 2:
      if (millis() - timerSrvCont > T_SRV_CONT) {
        Srv.setPWM(2, 0, SRV_CONT_MID);
        actionState[4] = 3;
      }
      break;
    case 3:
      //HOLA QUE TAL, ESTOY ACA HACIENDO NADA
      break;
  }
  Serial.println("luces");
  //luces grl
  Exp_brd.digitalWrite(1, actionState[5]);
  Exp_brd.digitalWrite(2, actionState[5]);
  Serial.println("react");
  //reactor
  Exp_brd.digitalWrite(6, actionState[6]);
  if ( actionState[6] > 0) {
    reactorFx(true);
  } else {
    reactorFx(false);
  }
  Serial.println("maqpb");
  //maq PB
  if ( actionState[7] > 0) {
    Exp_brd.digitalWrite(3, HIGH);
    Exp_brd.digitalWrite(4, HIGH);
  } else {
    Exp_brd.digitalWrite(3, LOW);
    Exp_brd.digitalWrite(4, LOW);
  }
  Serial.println("maqpa");
  //maq PA
  if ( actionState[8] > 0) {
    maqFx(true);
    Exp_brd.digitalWrite(5, HIGH);
  } else {
    maqFx(false);
    Exp_brd.digitalWrite(5, LOW);
  }

  // px1.show();
}

void reactorFx(bool state) {
  if (state > 0) {
    const byte animSteps = 6;
    byte fxStep = frame % animSteps;
    for (int i = 0; i < animSteps; i++) {
      if (fxStep == i) {
        px1.setPixelColor(i, px1.Color(255,   0,   0));

      } else {
        px1.setPixelColor(i, px1.Color(0,   0,   127));
      }
    }
  } else {
    for (int i = 0; i < 7; i++) {
      px1.setPixelColor(i, 0);
    }
  }
}

void maqFx(bool state) {
  if (state > 0) {
    const byte animSteps = 10;
    byte fxStep = frame % animSteps;
    px1.setPixelColor(6, px1.Color((255 / animSteps) * fxStep,   0,   100 - ((100 / animSteps) * fxStep)));
  } else {
    px1.setPixelColor(6, 0);
  }
}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    px1.show();
    frame++;
  }
}

//void doServoContinuo(unsigned long t0, unsigned long tx, int dir, int numServ) {
//  if (millis() - t0 < tx) {
//    //moverServo
//    if (dir > 0) {
//      Srv.setPWM(numServ, 0, SRV_CONT_MAX);
//    } else {
//      Srv.setPWM(numServ, 0, SRV_CONT_MIN);
//    }
//  } else {
//    Srv.setPWM(numServ, 0, 0);
//  }
//
//}
