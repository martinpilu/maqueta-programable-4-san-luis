/*
   Tablero 2, placa C.
   ver: 1.0.0
   Acciones:
   -- hito
   31 color luces

   -- terraza
    29 plataforma
    30 edificios
  plataforma: px count 7
  terraza 1: px count: 15 -- param: 1 on, 4 off
  terraza 2: px count:18 -- param: 2 on , 5 off
  terraza 3: px count: 16 -- param: 3 on , 6 off

  --ciudad
  18 Alumbrado publico
  19 escuela >>> pasa a luces locales
  20 iglesia >>> pasa a luces casas
  21 locales
  22 casas
  28 fuente

  exp1
  0 publico
  1 local
  2 local
  3 casa
  4 casa
  5 local
  6 casa
  7 local
  8 casa
  9 local
  10 casa
  11 casa
  12 local parrilla
  13 casa
  14 casa
  15 local
  exp2
  16 0 casa
  17 1 local
  18 2 locales hotel
  19 3 publico
  20 4 publico
  21 5 publico
  22 6 publico
  23 7 publico
  24 8 puclico
  25 9 local
  26 10 local
  27 11 casa
  28 12 casa
  29 13 local banco
  30 14 casa
  31 15 NC
*/
#include <Gallardcore_NanoExpander.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include "Adafruit_MCP23017.h"
#include <Adafruit_NeoPixel_NOT.h>
#include <avr/wdt.h>


#define PXLED_PIN 2 //Out 2

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 56 // 55 terrazas, 1 hito

Adafruit_NeoPixel px1(9, 7, NEO_BRG + NEO_KHZ800); //fuente
Adafruit_NeoPixel px2(LED_COUNT, PXLED_PIN, NEO_BRG + NEO_KHZ800);

Adafruit_MCP23017 Exp_brd_A;
Adafruit_MCP23017 Exp_brd_B;

Adafruit_PWMServoDriver Srv = Adafruit_PWMServoDriver();

NANO_EXPANDER Brd;

#define ACTION_COUNT 11
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {31, 29, 30, 18, 19, 20, 21, 22, 23, 24, 28};

#define LC_COUNT 12
#define CS_COUNT 12
#define PU_COUNT 7

byte locales[LC_COUNT] = {1, 2, 5, 7, 9, 12, 15, 17, 18, 25, 26, 29};
byte casas[CS_COUNT] = {3, 4, 6, 8, 10, 11, 13, 14, 16, 27, 28, 30};
byte publico[PU_COUNT] = {0, 19, 20, 21, 22, 23, 24};


unsigned long timer = 0;
const unsigned int frameTime = 100;
unsigned long frame = 0;

boolean lfoPause = false;
float lfo1 = 0.0;
int lfoINT = 0;
byte lfo1Dir = true;
float lfo1Rate = 3
                 ;
byte lfo1SQ = 0;

void setup() {
  wdt_disable();
#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif

  Brd.init();

  Exp_brd_A.begin();      // use default address 0
  Exp_brd_B.begin(1);      // use address 1

  for (int i = 0; i < 16; i++) {
    Exp_brd_A.pinMode(i, OUTPUT);
    Exp_brd_B.pinMode(i, OUTPUT);
    Exp_brd_A.digitalWrite(i, LOW);
    Exp_brd_B.digitalWrite(i, LOW);
  }

  Srv.begin();
  Srv.setPWMFreq(60);

  //px out1
  px1.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px1.setBrightness(255); //
  px1.show();            // Turn OFF all pixels ASAP
  //px out2
  px2.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px2.setBrightness(255); //
  px2.show();            // Turn OFF all pixels ASAP
  wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);
  frameCounter();
  //hito
  for (int i = 0; i < 3; i++) {
    Brd.setOut(i + 1, 255 * bitRead(actionState[0], i));
  }

  //terrazas
  lucesPlataforma(actionState[1]);
  lucesEdificios(actionState[2]);
  px1.show();
  px2.show();

  //ciudad luces
  Brd.setOut(4, actionState[6] * 255);
  Brd.setOut(5, actionState[7] * 255);
  lucesLocales(actionState[6]);
  lucesCasas(actionState[7]);
  lucesPublico(actionState[3]);

  //fuente
  fuente(actionState[10]);
}

void lucesLocales(byte state) {
  if (state > 0) {
    for (int i = 0; i < LC_COUNT; i++) {
      expCtrl(locales[i], HIGH);
    }
  } else {
    for (int i = 0; i < LC_COUNT; i++) {
      expCtrl(locales[i], LOW);
    }
  }
}

void lucesCasas(byte state) {
  if (state > 0) {
    for (int i = 0; i < CS_COUNT; i++) {
      expCtrl(casas[i], HIGH);
    }
  } else {
    for (int i = 0; i < CS_COUNT; i++) {
      expCtrl(casas[i], LOW);
    }
  }
}

void lucesPublico(byte state) {
  if (state > 0) {
    for (int i = 0; i < PU_COUNT; i++) {
      expCtrl(publico[i], HIGH);
    }
  } else {
    for (int i = 0; i < PU_COUNT; i++) {
      expCtrl(publico[i], LOW);
    }
  }
}

void expCtrl(byte out, byte val) {
  if (out <= 15) {
    Exp_brd_A.digitalWrite(out, val);
  } else {
    Exp_brd_B.digitalWrite(out - 15, val);
  }
}

void fuente(byte state) {
  if (state > 0) {
    lfoPause = false;
    Srv.setPWM(0, 0, map(lfoINT, 0, 255, 150, 550));
    //for (int i = 0; i < 9; i++) {
    px1.setPixelColor((frame) % 9, px1.Color(0,   255,   255));
    px1.setPixelColor(frame * 2 % 9, px1.Color(0,   0,   0));
    //}
  } else {
    lfoPause = true;
    //Srv.setPWM(0, 0, map(lfoINT, 0, 255, 150, 550));
    for (int i = 0; i < 9; i++) {
      px1.setPixelColor(i, 0);
    }
  }
}

void lucesPlataforma(byte state) {
  if (state > 0) {
    for (int i = 0; i < 7; i++) {
      px2.setPixelColor(i, px2.Color(255,   255,   255));
    }
  } else {
    for (int i = 0; i < 7; i++) {
      px2.setPixelColor(i, 0);
    }
  }
}

void lucesEdificios(byte state) {
  switch (state) {
    case 4:
      for (int i = 7; i < 7 + 15; i++) {
        px2.setPixelColor(i, 0);
      }
      break;
    case 1:
      for (int i = 7; i < 7 + 15; i++) {
        if (random(2) > 0) {
          px2.setPixelColor(i, px2.Color(255,   255,   255));
        } else {
          px2.setPixelColor(i, 0);
        }
      }
      break;
    case 5:
      for (int i = 22; i < 22 + 18; i++) {
        px2.setPixelColor(i, 0);
      }
      break;
    case 2:
      for (int i = 22; i < 22 + 18; i++) {
        if (random(2) > 0) {
          px2.setPixelColor(i, px2.Color(255,   255,   255));
        } else {
          px2.setPixelColor(i, 0);
        }
      }
      break;
    case 6:
      for (int i = 40; i < 40 + 16; i++) {
        px2.setPixelColor(i, 0);
      }
      break;
    case 3:
      for (int i = 40; i < 40 + 16; i++) {
        if (random(2) > 0) {
          px2.setPixelColor(i, px2.Color(255,   255,   255));
        } else {
          px2.setPixelColor(i, 0);
        }
      }
      break;
  }
  actionState[2] = 0;
}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    frame++;
  }
  if (!lfoPause) lfo1Run();
}

void lfo1Run() {
  Serial.println(lfoINT);

  if (lfo1Dir) {
    lfo1 += lfo1Rate;
  }
  else {
    lfo1 -= lfo1Rate;
  }

  if (lfo1 > 255) {
    lfo1Dir = false;
    lfo1 = 255;
  }

  if (lfo1 < 0) {
    lfo1Dir = true;
    lfo1 = 0;
  }
  if (lfo1 > 128) {
    lfo1SQ = 1;
  } else {
    lfo1SQ = 0;
  }
  lfoINT = int(lfo1);
}
