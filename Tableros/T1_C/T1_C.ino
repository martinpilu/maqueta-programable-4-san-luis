/*
   Tablero 1, placa C.
   ver: 1.0.0
   Acciones:
   -- Productividad
   42 luces corrales y estacion  OUT 1 y OUT 2
   54 luces productividad.
   --- brazos



*/
#include "rutinas.h"
#include <Gallardcore_NanoExpander.h>
#include <Gallardcore_5axis_servo.h>

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <avr/wdt.h>


NANO_EXPANDER Brd;

#define ACTION_COUNT 5
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {42, 54, 120, 121, 52};

//Brazos
Adafruit_PWMServoDriver servoA = Adafruit_PWMServoDriver(0x41);// 0x41 es el 1 (tren mas cercano a productividad)
Adafruit_PWMServoDriver servoB = Adafruit_PWMServoDriver(0x40);// 0x40 es el 2 (tren mas cercano a san luis)
Adafruit_PWMServoDriver servoC = Adafruit_PWMServoDriver(0x42);// 0x42 es el 3 (independiente)

Axis_Control arm_A;
Axis_Control arm_B;
Axis_Control arm_C;

boolean cargadoA = false;
boolean cargadoB = false;
boolean cargadoC = false;

void setup() {
    wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif

  //init: instancia del servo driver i2c, posicion de inicio.
  arm_A.init(servoA, &cargaBrazoA[0][0]);
  arm_B.init(servoB, &cargaBrazoB[0][0]);
  arm_C.init(servoC, &cargaBrazoC[0][0]);

//  Brd.sendAction(53, 1);
    wdt_enable(WDTO_2S);

}

void loop() {
      wdt_reset();

  arm_A.updateAxis();
  arm_B.updateAxis();
  arm_C.updateAxis();

  // leer mensajes del server.
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);

  //luces estacion y corrales
  Brd.setOut(1, actionState[0] * 255);
  Brd.setOut(2, actionState[0] * 255);

  //luces productividad
  Brd.setOut(3, actionState[1] * 255);

  //brazos
  switch (actionState[2]) {
    case 0:
      //idle
      break;
    case 1:
      if (arm_A.rutineState()) {
        if (!cargadoA) {
          cargadoA = true;
          arm_A.doRutine(&cargaBrazoA[0][0], 11);
        } else {
          cargadoA = false;
          arm_A.doRutine(&descargaBrazoA[0][0], 11);
        }
      }
      actionState[2] = 3;
      break;
    case 3:
      //running
      if (arm_A.rutineState()) {
        //enviar idle
        Brd.sendAction(120, 0);
        actionState[2] = 0;
      }
      break;
  }

  switch (actionState[3]) {
    case 0:
      //idle
      break;
    case 1:
      if (arm_B.rutineState()) {
        if (!cargadoB) {
          cargadoB = true;
          arm_B.doRutine(&cargaBrazoB[0][0], 11);
        } else {
          cargadoB = false;
          arm_B.doRutine(&descargaBrazoB[0][0], 11);
        }
      }
      actionState[3] = 3;
      break;
    case 3:
      //running
      if (arm_B.rutineState()) {
        //enviar idle
        Brd.sendAction(121, 0);
        actionState[3] = 0;
      }
      break;
  }

  switch (actionState[4]) {
    case 0:
      //idle
      break;
    case 1:
      if (arm_C.rutineState()) {
        if (!cargadoC) {
          cargadoC = true;
          arm_C.doRutine(&cargaBrazoC[0][0], 11);
        } else {
          cargadoC = false;
          arm_C.doRutine(&descargaBrazoC[0][0], 11);
        }
      }
      actionState[4] = 3;
      break;
    case 3:
      //running
      if (arm_C.rutineState()) {
        actionState[4] = 0;
      }
      break;
  }

}
