/*
   Tablero 4, placa C.
   ver: 1.0.0
   Acciones:
   -- hidrica dique.
   67 barco laguna
   69 pinturas rupestres

*/

#include <Gallardcore_NanoExpander.h>
#include <avr/wdt.h>


NANO_EXPANDER Brd;

Desplazamiento barco(A0, A1, OUT5, OUT4, 255);

#define ACTION_COUNT 3
uint8_t actionState[ACTION_COUNT] = {0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {67, 68, 69};

unsigned long timer = 0;
const unsigned int frameTime = 2;
unsigned long frame = 0;

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif

  Brd.init();
  wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();
  frameCounter();
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);
  barco.tick();

  if (actionState[0] > 0) {
    actionState[0] = 0;
    barco.go();
  }

  //Luces rupestres
  Brd.setOut(3, actionState[2] * 255);
}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    frame++;
  }
}
