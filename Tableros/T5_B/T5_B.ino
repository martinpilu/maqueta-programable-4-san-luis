/*
   Tablero 5, placa B.
   ver: 1.0.0
   Acciones:
   -- villa mercedes
   82 alumbrado publico v mcd o5
   83 casas v mcd  04
   91 teatro o3 y pix1

   -- pedrera
   90 autodromo autos o1
   92 autodromo luces o6
   88 estadio o2


   -- general
   4 a 17 wifi pix2


*/

#include <Gallardcore_NanoExpander.h>
#include <Adafruit_NeoPixel_NOT.h>
#include <avr/wdt.h>


NANO_EXPANDER Brd;

#define PXLED_PIN 7 //px Out 1
#define PXLED_PIN2 2 //px Out 2
#define WIFI_COUNT 26 //cantidad gotitas wifi
#define LED_COUNT 4

Adafruit_NeoPixel px1(LED_COUNT, PXLED_PIN, NEO_BRG + NEO_KHZ800); //fuente
Adafruit_NeoPixel px2(WIFI_COUNT, PXLED_PIN2, NEO_BRG + NEO_KHZ800); //fuente


#define ACTION_COUNT 20
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {82, 83, 91, 92, 88, 90, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};

uint8_t cantWifixZona [14] = {3, 3, 2, 2, 1, 1, 2, 2, 2, 1, 2, 2, 3, 0};

unsigned long timer = 0;
const unsigned int frameTime = 100;
unsigned long frame = 0;

float lfo1 = 0.0;
int lfoINT = 0;
byte lfo1Dir = true;
float lfo1Rate = 1.4;
byte lfo1SQ = 0;

//int autoSpeed = 50;
//int sensorPinA = A0;

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif

  //pinMode(4, OUTPUT);
  //digitalWrite(4, HIGH);

  Brd.init();

  //px out1
  px1.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px1.setBrightness(255); //
  px1.show();            // Turn OFF all pixels ASAP
  px2.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px2.setBrightness(255);
  px2.show();            // Turn OFF all pixels ASAP

wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();

  frameCounter();
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);

  //v mcds publico 82
  Brd.setOut(5, actionState[0] * 255);
  //casas mcds 83
  Brd.setOut(4, actionState[1] * 255);

  //teatro
  Brd.setOut(3, actionState[2] * 255);
  lucesTeatro(actionState[2]);

  //Luz autodromo
  Brd.setOut(1, actionState[3] * 255);

  //Luz estadio
  Brd.setOut(2, actionState[4] * 255);
  //Autos autodromo
  Brd.setOut(6, actionState[5]*255);
  /*
    int sensorValueA = analogRead(sensorPinA);

    switch (actionState[5]) {
    case 0:
      Brd.setOut(6, 0);
      break;
    case 1:
      Brd.setOut(6, 120);
      delay(100); // es un toque no pasa na, solo para darle un empujoncito al auto.
      Brd.setOut(6, actionState[5] * map(sensorValueA, 0, 1024, 0, 255));
      actionState[5] = 2;
      break;
    case 2:
      Brd.setOut(6, actionState[5] * map(sensorValueA, 0, 1024, 0, 255));
      break;
    }
    //sensorValueA = analogRead(sensorPinA);
    //Brd.setOut(6, actionState[5] * map(sensorValueA, 0, 1024, 0, 255));
  */


  //WIFI

  for (int i = 0; i < WIFI_COUNT; i++) {
    for (int j = 0; j < 14; j++) {
      for (int k = 0; k < cantWifixZona[j]; k++) {
        if (actionState[6 + j] > 0) {
          if (i == 18) {
            px2.setPixelColor(20, px2.Color(0, lfoINT, 0));
          } else if (i == 20) {
            px2.setPixelColor(18, px2.Color(0, lfoINT, 0));
          } else {
            px2.setPixelColor(i, px2.Color(0, lfoINT , 0));
          }
        } else {
          if (i == 18) {
            px2.setPixelColor(20, px2.Color(0,   0,   0));
          } else if (i == 20) {
            px2.setPixelColor(18, px2.Color(0,  0,   0));
          } else {
            px2.setPixelColor(i, px2.Color(0,  0 ,   0));
          }
        }
        i++;
      }
    }
  }

  px2.show();
  px1.show();
}

void lucesTeatro(byte state) {
  if (state > 0) {
    const byte animSteps = 9;
    byte fxStep = frame % animSteps;
    for (int i = 0; i < animSteps; i++) {
      if (fxStep == i) {
        if (state < 2) {
          px1.setPixelColor(i, px1.Color(255,   255,   255));
        } else {
          px1.setPixelColor(i, px1.Color(255 * lfo1SQ,  255 * lfo1SQ,   255 * lfo1SQ));
        }
      } else {
        if (state < 3) {
          px1.setPixelColor(i, px1.Color(0,   50,   0));
        } else {
          px1.setPixelColor(i, px1.Color(0,   50 * lfo1SQ,   0));
        }
      }
    }
  } else {
    for (int i = 0; i < LED_COUNT; i++) {
      px1.setPixelColor(i, 0);
    }
  }
}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    frame++;
  }
  lfo1Run();
}

void lfo1Run() {
  // Serial.println(lfoINT);

  if (lfo1Dir) {
    lfo1 += lfo1Rate;
  }
  else {
    lfo1 -= lfo1Rate;
  }

  if (lfo1 > 255) {
    lfo1Dir = false;
    lfo1 = 255;
  }

  if (lfo1 < 0) {
    lfo1Dir = true;
    lfo1 = 0;
  }
  if (lfo1 > 128) {
    lfo1SQ = 1;
  } else {
    lfo1SQ = 0;
  }
  lfoINT = int(lfo1);
}
