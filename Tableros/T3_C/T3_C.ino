/*
  Tablero 3, placa C.
  ver: 1.0.0
  Acciones:
  --escuela
  70 luces
  71 niños
  72 bandera
  --casa futuro
  73  Encendido de panel solar (ya no, pasó a T3_A)
  74  Encendido Luz PB Sobre heladera
  75  Encendido Horno
  76  Encendido Heladera
  77  Encendido Hornallas
  78  Encendido Luces Baño (Espejo)
  79  Encendido Luces Habitacion (TV, Hogar, Respaldo Cama)
  80  Encendido Luz Pileta
  81  Encendido Luz Escalera

  exp1
  0 espejo
  1 tele
  2 cama
  3 escalera
  4 hornallas
  5 horno
  6 heladera 1
  7 heladera 2
  8 luz sobre heladera
  9 pileta
  10 escuela blanca
  11 NC
  12 NC
  13 NC
  14 NC
  15 NC

*/
#include <Gallardcore_NanoExpander.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include "Adafruit_MCP23017.h"
#include <avr/wdt.h>


Adafruit_MCP23017 Exp_brd;

Adafruit_PWMServoDriver Srv = Adafruit_PWMServoDriver();

NANO_EXPANDER Brd;
//Desplazamiento(uint8_t homeSensorPin, uint8_t endSensorPin, uint8_t motorPinA, uint8_t motorPinB, uint8_t speed){

Desplazamiento bandera(A0, A1, OUT4, OUT5, 255);

#define ACTION_COUNT 12
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {200, 74, 75, 76, 77, 78, 79, 80, 81, 71, 70, 72};

unsigned long timer = 0;
const unsigned int frameTime = 100;
unsigned long frame = 0;

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif
  Exp_brd.begin();      // use default address 0
  for (int i = 0; i < 16; i++) {
    Exp_brd.pinMode(i, OUTPUT);
    Exp_brd.digitalWrite(i, LOW);
  }
  Brd.init();
  Srv.begin();
  Srv.setPWMFreq(60);
  //bandera.go();
  wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();
  frameCounter();
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);
  bandera.tick();

  //tirolesa
  if (actionState[11] > 0) {
    actionState[11] = 0;
    bandera.go();
  }
  //objetos casa
  //luz sobre heladera
  Exp_brd.digitalWrite(8, actionState[1]);
  //horno
  Exp_brd.digitalWrite(5, actionState[2]);
  //heladera
  Exp_brd.digitalWrite(6, actionState[3]);
  Exp_brd.digitalWrite(7, actionState[3]);
  //hornallas
  Exp_brd.digitalWrite(4, actionState[4]);
  //baño
  Exp_brd.digitalWrite(0, actionState[5]);
  //habitacion
  Exp_brd.digitalWrite(1, actionState[6]);
  Exp_brd.digitalWrite(2, actionState[6]);
  //pileta
  Exp_brd.digitalWrite(9, actionState[7]);
  //escalera
  Exp_brd.digitalWrite(3, actionState[8]);


  //esuela
  Exp_brd.digitalWrite(10, HIGH);
  //ñato
  Brd.setOut(6, actionState[9] * 50);

  //mural
  mural(actionState[10]);

  //Servo paneles solares
  //setServo(0, actionState[0]);
}

void mural(byte state) {
  switch (state) {
    case 0:
      Brd.setOut(1, 0);
      Brd.setOut(2, 0);
      Brd.setOut(3, 0);
      break;
    case 1:
      Brd.setOut(1, 255);
      Brd.setOut(2, 0);
      Brd.setOut(3, 0);
      break;
    case 2:
      Brd.setOut(1, 0);
      Brd.setOut(2, 255);
      Brd.setOut(3, 0);
      break;
    case 3:
      Brd.setOut(1, 0);
      Brd.setOut(2, 0);
      Brd.setOut(3, 255);
      break;
    case 4:
      const byte animSteps = 3;
      byte fxStep = frame % animSteps;
      for (int i = 0; i < animSteps; i++) {
        if (fxStep == i) {
          Brd.setOut(i + 1, 255);
        } else {
          Brd.setOut(i + 1, 0);
        }
      }
      break;
  }
}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    frame++;
  }
}


void setServo(byte num, byte angle) {
  //Srv.setPWM(num, 0, map(angle, 0, 4, 170, 559));
}
