/*
   Tablero 5, placa C.
   ver: 1.0.0
   Acciones:
   -- mercedes
   86 barrera (i2c 0)
   109 autopista o5

   --pedrera
   93 alumbrado publico o1
   87 casas 03
   110 autopista o6
   
   
   

*/

#include <Gallardcore_NanoExpander.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <avr/wdt.h>


NANO_EXPANDER Brd;

Adafruit_PWMServoDriver Srv = Adafruit_PWMServoDriver();
#define BARRERA_MAX 80
#define BARRERA_MIN 165

#define PXLED_PIN 7 //px Out 1
#define LED_COUNT 200

#define ACTION_COUNT 6
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {109, 110, 93, 87, 86, 34};

unsigned long timer = 0;
const unsigned int frameTime = 2;
unsigned long frame = 0;

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif

  Brd.init();

   // servo
  Srv.begin();
  Srv.setPWMFreq(50);

wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();
  frameCounter();
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);

  //Luces de autopista
  Brd.setOut(5, actionState[0] * 255);
  Brd.setOut(6, actionState[1] * 255);

  //alumbrado pedrera
  Brd.setOut(1, actionState[2] * 255);

  //casas pedrera
  Brd.setOut(3, actionState[3] * 255);

  //barrera actionState[4]
  if (actionState[4]>0) {
    Srv.setPWM(0, 0, BARRERA_MAX);
  } else {
    Srv.setPWM(0, 0, BARRERA_MIN);
  }


}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    frame++;
  }
}
