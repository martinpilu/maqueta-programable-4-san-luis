/*
  Tablero 2, placa A.
   ver: 1.0.0
   Acciones:
   -- San Luis
   26 autos o4, o5
   27 semaforo o1 rojo, o2 amarillo , o3 verde

*/

#include <Gallardcore_TCP_Protocol.h>
#include <Gallardcore_NanoExpander.h>
#include <avr/wdt.h>

#define ROJO 1
#define AMARILLO 2
#define VERDE 3

TCPcomm TCP;
NANO_EXPANDER Brd;

uint8_t mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x02};

#define ACTION_COUNT 2
uint8_t actionState[ACTION_COUNT] = {0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {26, 27};

unsigned long timer = 0;
const unsigned int frameTime = 100;
unsigned long frame = 0;

int semaforoStep = 0;

void setup() {
  wdt_disable();
#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
#endif
  /// mac, Arduino Ip, Server Ip
  TCP.init(mac, IPAddress(10, 75, 33, 112), IPAddress(10, 75, 33, 101), 9001);
  Brd.init();

  //autos corriendo siempre, comanda semaforo.
  //Brd.setOut(4, 255);
  wdt_enable(WDTO_2S);
}

void loop() {
  wdt_reset();
  frameCounter();
  TCP.parse(actionState, actionNumber, ACTION_COUNT);

  // action 26 activar autos
  switch (actionState[0]) {
    case 0:
      Brd.setOut(4, 0);
      break;
    case 1:
      Brd.setOut(4, 255);
      break;
  }

  //semaforo
  semaforo(actionState[1]);

  //action 27 semaforo zona de detencion
  if (actionState[1] > 0) {
    //activar zona de frenado si action 26 esta on

  } else {
    //detener zona
    Brd.setOut(5, 0);
  }


}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    frame++;
  }
}

void semaforo(byte state) {
  if (frame > 10) {
    if (state > 0) {
      semaforoStep ++;
    }
    else {
      semaforoStep --;
    }
    if (semaforoStep > 2) semaforoStep = 3;
    if (semaforoStep < 0) semaforoStep = -1;

    frame = 0;
  }

  switch (semaforoStep) {
    case 0:
      Brd.setOut(ROJO, 100);
      Brd.setOut(AMARILLO, 0);
      Brd.setOut(VERDE, 0);
      break;
    case 1:
      Brd.setOut(ROJO, 0);
      Brd.setOut(AMARILLO, 255);
      Brd.setOut(VERDE, 0);
      break;
    case 2:
      Brd.setOut(ROJO, 0);
      Brd.setOut(AMARILLO, 0);
      Brd.setOut(VERDE, 255);
      break;
  }
  //activar zona auto si esta en verde
  if (actionState[0] > 0 && semaforoStep > 1) {
    Brd.setOut(5, 255);
  } else {
    Brd.setOut(5, 0);
  }
}
