// Tablero 1 palaca B
//
#include <Gallardcore_NanoExpander.h>
#include <Adafruit_NeoPixel_NOT.h>
#include <avr/wdt.h>


#define PXLED_PIN 7 //Out 1

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 10

Adafruit_NeoPixel px1(LED_COUNT, PXLED_PIN, NEO_BRG + NEO_KHZ800); //fuente

NANO_EXPANDER Brd;
//Desplazamiento(uint8_t homeSensorPin, uint8_t endSensorPin, uint8_t motorPinA, uint8_t motorPinB, uint8_t speed){

Desplazamiento tirolesa(A2, A3, OUT2, OUT3, 255);
Desplazamiento pala(A0, A1, OUT4, OUT5, 255);

#define ACTION_COUNT 6
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {61, 62, 63, 64, 65, 160};

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif
  Brd.init();

  //px out1
  px1.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px1.setBrightness(255); //
  px1.show();            // Turn OFF all pixels ASAP
  wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);
  tirolesa.tick();
  pala.tick();

  //tirolesa
  if (actionState[1] > 0) {
    actionState[1] = 0;
    tirolesa.go();
  }

  //pala
  if (actionState[3] > 0) {
    actionState[3] = 0;
    pala.go();
  }
  
  //luz fosiles
  fosiles(actionState[0]);

  //transportadora
  Brd.setOut(6, actionState[2] * 60);

  //pileton
  Brd.setOut(1, actionState[4] * 255);

  px1.show();
}

void fosiles (byte state) {
  switch (state) {
    case 0:
      for (int i = 0; i < LED_COUNT; i++) {
        px1.setPixelColor(i, 0);
      }
      break;
    case 1:
      for (int i = 0; i < LED_COUNT; i++) {
        px1.setPixelColor(i, px1.Color(0,   255,   255));
      }
      break;
    case 2:
      for (int i = 0; i < LED_COUNT; i++) {
        px1.setPixelColor(i, px1.Color(255,   0,   255));
      }
      break;
    case 3:
      for (int i = 0; i < LED_COUNT; i++) {
        px1.setPixelColor(i, px1.Color(255,   255,   0));
      }
      break;
  }
}
