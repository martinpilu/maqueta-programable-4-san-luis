/*
   Tablero 4, placa A.
   ver: 1.0.0
   Acciones:
    General
    luces autopista: 107 - 108
    Merlo
    Arbol: 46
    parapente: 47
   */

#include <Gallardcore_TCP_Protocol.h>
#include <Gallardcore_NanoExpander.h>
#include <avr/wdt.h>


TCPcomm TCP;
NANO_EXPANDER Brd;

#define ACTION_COUNT 4
uint8_t mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x04};
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {107, 108, 46, 47};

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
#endif
  /// mac, Arduino Ip, Server Ip
  TCP.init(mac, IPAddress(10, 75, 33, 114), IPAddress(10, 75, 33, 101), 9001);
  Brd.init();
  wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();
  TCP.parse(actionState, actionNumber, ACTION_COUNT);

  //luces autopista
  Brd.setOut(1, actionState[0] * 255);
  Brd.setOut(2, actionState[1] * 255);

  //arbol
  switch (actionState[2]) {
    case 0:
      Brd.setOut(3, 0);

      break;
    case 1:
      Brd.setOut(3, 130);
      delay(30);
      actionState[2] = 2;
      break;
    case 2:
      Brd.setOut(3, 50);
      break;
  }


  //parapente
  Brd.setOut(4, actionState[3] * 40);
}
