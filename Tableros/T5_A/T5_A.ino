/*
   Tablero 5, placa C.
   ver: 1.0.0
   Acciones:
   -- aeropuerto
   85 semaforo
   84 autos
   3 fibra optica
*/

#include <Gallardcore_TCP_Protocol.h>
#include <Gallardcore_NanoExpander.h>
#include <Adafruit_NeoPixel_NOT.h>
#include <avr/wdt.h>

TCPcomm TCP;
uint8_t mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x06};
NANO_EXPANDER Brd;

#define PXLED_PIN 7 //px Out 1
#define LED_COUNT 200

Adafruit_NeoPixel px1(LED_COUNT, PXLED_PIN, NEO_BRG + NEO_KHZ800); //fuente

#define ACTION_COUNT 3
uint8_t actionState[ACTION_COUNT] = {0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {3, 85, 84};

unsigned long timer = 0;
const unsigned int frameTime = 2;
unsigned long frame = 0;
unsigned long frameB = 0;

boolean sensASent = false;
boolean sensBSent = false;

int semaforoStep = 0;

#define ROJO 1
#define AMARILLO 2
#define VERDE 3

#define ESPACIO_FIBRA 30

void setup() {
  wdt_disable();
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif
  TCP.init(mac, IPAddress(10, 75, 33, 116), IPAddress(10, 75, 33, 101), 9001);
  Brd.init();


  //px out1
  px1.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px1.setBrightness(255); //
  px1.show();            // Turn OFF all pixels ASAP

  Brd.setOut(4, 255); //autos solo detienen en area semaforo.
  wdt_enable(WDTO_2S);
}

void loop() {
  wdt_reset();
  frameCounter();
  //Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);
  TCP.parse(actionState, actionNumber, ACTION_COUNT);

  // action 84 activar autos
  //  switch (actionState[2]) {
  //    case 0:
  //      Brd.setOut(4, 0);
  //      break;
  //    case 1:
  //      Brd.setOut(4, 255);
  //      break;
  //  }

  //semaforo
  semaforo(actionState[1]);

  //action 27 semaforo zona de detencion
  if (actionState[1] > 0) {
    //activar zona de frenado si action 26 esta on

  } else {
    //detener zona
    Brd.setOut(5, 0);
  }

  if (analogRead(A0) > 200 && !sensASent) {
    Serial.println("sens 6");
    TCP.sendAction(53, 10);
    sensASent = true;
  }
  if (analogRead(A0) < 200) {
    sensASent = false;
  }

  if (analogRead(A1) > 200 && !sensBSent) {
    Serial.println("sens 7");
    TCP.sendAction(53, 11);
    sensBSent = true;
  }
  if (analogRead(A1) < 200) {
    sensBSent = false;
  }
}

void mandaLaData(int cant) {
  int i = frame % (200);
  for (int j = 0; j < cant; j++) {
    int prev = i - 2 + (j * ESPACIO_FIBRA);
    if (prev == -1) prev = 198;
    if (prev == -2) prev = 199;
    px1.setPixelColor(prev, px1.Color(0,   0,   0));
    px1.setPixelColor(i + (j * ESPACIO_FIBRA), px1.Color(0,   50,   255));
  }
}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    frame++;
    frameB ++;

    // firbra en frame por que frena todo sino
    switch (actionState[0]) {
      case 0:
        px1.clear();
        px1.show();
        actionState[0] = 10;
        break;
      case 1:
        mandaLaData(1);
        px1.show();
        break;
      case 2:
        mandaLaData(2);
        px1.show();
        break;
      case 3:
        mandaLaData(3);
        px1.show();
        break;
      case 4:
        mandaLaData(4);
        px1.show();
        break;
      case 10:
        break;
    }
  }
}

void semaforo(byte state) {
  if (frameB > 500) {
    if (state > 0) {
      semaforoStep ++;
    }
    else {
      semaforoStep --;
    }
    if (semaforoStep > 2) semaforoStep = 3;
    if (semaforoStep < 0) semaforoStep = -1;

    frameB = 0;
  }

  switch (semaforoStep) {
    case 0:
      Brd.setOut(ROJO, 100);
      Brd.setOut(AMARILLO, 0);
      Brd.setOut(VERDE, 0);
      break;
    case 1:
      Brd.setOut(ROJO, 0);
      Brd.setOut(AMARILLO, 255);
      Brd.setOut(VERDE, 0);
      break;
    case 2:
      Brd.setOut(ROJO, 0);
      Brd.setOut(AMARILLO, 0);
      Brd.setOut(VERDE, 255);
      break;
  }
  //activar zona auto si esta en verde
  if (actionState[1] > 0 && semaforoStep > 2) {
    Brd.setOut(5, 255);
  } else {
    Brd.setOut(5, 0);
  }

}
