**Maqueta programable San Luis 4.0. - Programación de Arduinos -**

En ese git se encuentra la documentación técnica referida a mantenimiento y programación de los “objetos programables” de la maqueta. El conjunto de programas que aquí se disponen responden a: la implementación del accionamiento de objetos programables mediante la API para control web.

Para informacion general del proyecto https://gitlab.com/tomasciccola/sanLuisMaquetasDocumentacion

El sistema está compuesto por un conjunto de arduinos que escuchan mensajes de accion enviados por la API. 
Al recibir un mensaje de acción que les corresponda, ejecutan el programa que activa el sistema correspondiente al objeto programable.

Cada arduino está montado en una placa de expansión “Nano Expander” que lo provee de interfaz con los sistemas de cada objeto programable (sensores, motores, iluminación (leds), servos, tren, autos, etc.).
Estas placas están distribuidas de a 3 en 5 tableros seccionales. 
En cada uno, una placa provee comunicación ethernet a las otras dos replicando los mensajes por serie. La comunicación con la API web es mediante protocolo TCP y responden a la estructura definida en la documentacion 

**Guia**:

En la carpeta “**Tableros**” están los programas para cada una de las placas distribuidas en 5 tableros seccionales debajo de la maqueta.

La nomenclatura de los archivos corresponde a: TX Y donde X es el número de tablero e Y es la letra identificadora de cada placa.

Ej:
 T1_A: Placa de control del tren.


Referirse a la tabla de conexiones y diagramas en bloques para identificar cada arduino, placa y conexión. 


Dentro de la carpeta “**libraries**” se encuentran las librerías necesarias para compilar los proyectos. 


En la librería nano Expander se encuentran los ejemplos para realizar pruebas de diagnóstico de las distintas partes de la maqueta.


Prueba de sensores Ejemplo: inputTest.


Prueba de salidas de pixelLed Ejemplo: px Test


Prueba de salidas en placas. Ejemplo: expansoras. Expander_test


En librería 5 axis-arm-test se encuentra el ejemplo para controlar los brazos robóticos mediante usb.


Para información detallada de configuraciones aplicadas a cada objeto programable de la maqueta: referirse al programa correspondiente según número de tablero y letra de placa. 
****En cada programa se encuentran comentarios con información adicional.



