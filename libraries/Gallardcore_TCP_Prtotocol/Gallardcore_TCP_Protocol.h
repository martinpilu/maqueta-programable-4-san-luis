/*
*/

#ifndef Gallardcore_TCP_Protocol_h
#define Gallardcore_TCP_Protocol_h

#include "Arduino.h"
//

#include "HardwareSerial.h"
#include <UIPEthernet.h>
// #include "utility/logging.h"

class TCPcomm
{
public:
  TCPcomm();
  init(uint8_t *mac,IPAddress myIp,IPAddress serverIp,uint16_t port);
  parse(uint8_t *stateBuf,uint8_t *actionNumbers,uint8_t actionCount);
  sendAction(uint8_t actNum, uint8_t parm);

private:
  connect();
  uint32_t _next;
  IPAddress _serverIp;
  uint16_t _port;
};

#endif
