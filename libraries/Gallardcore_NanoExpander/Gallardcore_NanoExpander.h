/*
*/

#ifndef Gallardcore_NanoExpander_h
#define Gallardcore_NanoExpander_h

#include "Arduino.h"
#include "HardwareSerial.h"
// #include <UIPEthernet.h>
#include "utility/logging.h"

#define OUT1 3
#define OUT2 5
#define OUT3 6
#define OUT4 9
#define OUT5 10
#define OUT6 11

class NANO_EXPANDER
{
public:
  init();
  setOut(uint8_t out, uint8_t val);
  getIn();
  getRawIn();
  sensorsRead();
  parseSerial(uint8_t *stateBuf,uint8_t *actionNumbers,uint8_t actionCount);
  sendAction(uint8_t actNum, uint8_t parm);
};

class Desplazamiento
{
public:
  Desplazamiento(uint8_t homeSensorPin, uint8_t endSensorPin, uint8_t motorPinA, uint8_t motorPinB, uint8_t speed);
  tick();
  go();
  state();
private:
  const uint8_t _sensorTreshold = 200;
  uint8_t _state;
  uint32_t _timeout = 5000;
  uint32_t _timer;
  uint8_t _speed;
  uint8_t _motorPinA;
  uint8_t _motorPinB;
  uint8_t _homeSensorPin;
  uint8_t _endSensorPin;
};


#endif
