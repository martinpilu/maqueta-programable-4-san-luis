// These constants won't change. They're used to give names to the pins used:
const int analogInPin[6] = {A0, A1, A2, A3, A6, A7}; // Analog input pin that the potentiometer is attached to

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);
}

void loop() {
  for (int i = 0; i < 6; i++) {
    // print the results to the Serial Monitor:
    Serial.print(i+1);
    Serial.print(" = ");
    Serial.print(analogRead(analogInPin[i]));
    Serial.print(" | ");
  }
  Serial.println();
  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);
}
