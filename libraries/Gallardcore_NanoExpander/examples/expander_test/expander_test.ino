#include <Wire.h>
#include "Adafruit_MCP23017.h"

// Basic pin reading and pullup test for the MCP23017 I/O expander
// public domain!

// Connect pin #12 of the expander to Analog 5 (i2c clock)
// Connect pin #13 of the expander to Analog 4 (i2c data)
// Connect pins #15, 16 and 17 of the expander to ground (address selection)
// Connect pin #9 of the expander to 5V (power)
// Connect pin #10 of the expander to ground (common ground)
// Connect pin #18 through a ~10kohm resistor to 5V (reset pin, active low)

// Output #0 is on pin 21 so connect an LED or whatever from that to ground

Adafruit_MCP23017 mcp;
Adafruit_MCP23017 mcp2;

void setup() {
  Serial.begin(9600);
  mcp.begin();      // use default address 0
  mcp2.begin(1);      // use default address 0
  for (int i = 0; i < 16; i++) {
    mcp.pinMode(i, OUTPUT);
    mcp2.pinMode(i, OUTPUT);
  }
//  for (int i = 0; i < 16; i++) {
//    mcp.digitalWrite(i, LOW);
//    mcp2.digitalWrite(i, LOW);
//  }
  for (int i = 0; i < 16; i++) {
    mcp.digitalWrite(i, HIGH);
    mcp2.digitalWrite(i, HIGH);
    //delay(1000);
  }
}


// flip the pin #0 up and down

void loop() {

  if (Serial.available() > 0) {
    int out = Serial.parseInt();
    int val = Serial.parseInt();
    Serial.print(out);
    Serial.print(" = ");
    Serial.println(val);

    if (out == 40) {
      for (int i = 0; i < 16; i++) {
        mcp.digitalWrite(i, HIGH);
        mcp2.digitalWrite(i, HIGH);
        //delay(1000);
      }
    }

    if (out == 41) {
      for (int i = 0; i < 16; i++) {
        mcp.digitalWrite(i, LOW);
        mcp2.digitalWrite(i, LOW);
        //delay(1000);
      }
    }

    if (out <= 15) {
      for (int i = 0; i < 16; i++) {
        if (out == i) {
          mcp.digitalWrite(i, HIGH);
        } else {
          mcp.digitalWrite(i, LOW);
        }
      }
    }
    if (out > 15 && out < 32) {
      for (int i = 0; i < 16; i++) {
        if (out - 15 == i) {
          mcp2.digitalWrite(i, HIGH);
        } else {
          mcp2.digitalWrite(i, LOW);
        }
      }
    }
  }
}
